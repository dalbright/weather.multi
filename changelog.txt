v1.0.27
- disable cookie expiration
- Yahoo no longer provides weather data to new users

v1.0.26
- language update

v1.0.25
- reinit session with each new url

v1.0.24
- try multiple yahoo urls in order to get cookie

v1.0.23
- fix yahoo weather again

v1.0.22
- fix weatherbit ozone value could be none

v1.0.21
- fix getting yahoo cookie

v1.0.20
- fix weatherbit

v1.0.19
- fix yahoo weather again

v1.0.18
- fixed en_gb/strings.po translation

v1.0.17
- fix yahoo weather again

v0.0.16
- fix yahoo weather again

v0.0.15
- bump for repo release

v0.0.14
- fix yahoo weather again

v0.0.13
- fix yahoo weather again

v1.0.12
- merge weather.multi fork changes (see below) and apply to providers

v0.0.12
- language update

v0.0.11
- fix yahoo crumb format

v0.0.10
- fix weatherbit.io outlook translation
- add yahoo outlook translation

v0.0.9
- bump for repo release

v0.0.8
- fix fetching weather for wrong location

v0.0.7
- fix yahoo weather again

v0.0.6
- fix yahoo weather

v0.0.5
- fix crash when precipitation data is missing

v0.0.4
- fix location search
- fix yahoo daily weather

v0.0.3
- fix maps

v0.0.2
- refactor code

v0.0.1
- initial release
